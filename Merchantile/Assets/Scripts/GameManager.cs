﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public GameObject CatalogPanel;
    public GameObject Shop;
    public GameObject FloorGrid;
    public GameObject LevelManager;
    public Text MoneyText;
    public Text RemainingSpace;
    public Text EventText;

    const int smallCost = 10;
    const int mediumCost= 25;
    const int largeCost = 50;

    int EXPANSIONCOST = 100;

    int _money;
    public int Money
    {
        get { return _money; }
        set
        {
            _money = value;
            MoneyText.text = string.Format("Money: {0}", _money);
        }
    }


	// Use this for initialization
	void Start () {
        Money = 20;
        RemainingSpace.text = string.Format("Space: {0}", Shop.GetComponent<Shop>().RemainingSpace);
        EventText.text = "No buyers yet.";
        InvokeRepeating("RequestSellItem", 4, 5);
        //CreateShop();
    }

    // Update is called once per frame
    void Update () {
	}


    //side menu buttons
    public void RequestOpenCatalog()
    {
        print("Open Catalog requested");
        CatalogPanel.gameObject.SetActive(true);
    }

    public void RequestSellItem()
    {
        print("sell items requested");
        EventText.text = "New buyer appeared";

        var sizes = new List<ItemSize>() { ItemSize.Small, ItemSize.Medium, ItemSize.Large };
        var randSize = Random.Range(0, sizes.Count);
        var selectedSize = sizes[randSize];
        //TODO: Remove this comment when determined-buys are added
        //for now just sell a random item, can finesse it later

        var items = Shop.GetComponent<Shop>().Items;
        var itemsOfSize = new List<Item>();
        foreach(var item in items)
        {
            if (item.Size == selectedSize)
            {
                itemsOfSize.Add(item);
            }
        }
        if(itemsOfSize.Count == 0) //nothing to sell
        {
            EventText.text = string.Format("A customer came by looking for an item of size: {0}", selectedSize.ToString());
            return;
        }
        var randInt = Random.Range(0, itemsOfSize.Count - 1);

        var itemToSell = itemsOfSize[randInt];
        var result = Shop.GetComponent<Shop>().SellItem(itemToSell);
        Money += result;
        RemainingSpace.text = string.Format("Space: {0}", Shop.GetComponent<Shop>().RemainingSpace);
        EventText.text = string.Format("A customer bought an item of size: {0}", itemToSell.Size.ToString());
        //TODO: Update text for which size was purchased or not


    }

    public void ExpandShop()
    {
        _TryExpandShop();
    }

    public void WinGame()
    {
        LevelManager.GetComponent<LevelManager>().LoadLevel("Win");
    }


    //catalog
    public void CloseCatalog()
    {
        CatalogPanel.gameObject.SetActive(false);
    }

    public void BuySmall()
    {
        var size = ItemSize.Small;
        _TryBuyItem(size, smallCost);
    }

    public void BuyMedium()
    {
        var size = ItemSize.Medium;
        _TryBuyItem(size, mediumCost);
    }

    public void BuyLarge()
    {
        var size = ItemSize.Large;
        _TryBuyItem(size, largeCost);
    }

    bool _ShopHasSpace(ItemSize size)
    {
        print("space" + Shop.GetComponent<Shop>().RemainingSpace);
        return Shop.GetComponent<Shop>().RemainingSpace >= (int)size; //change this eventually maybe.....to square the value
    }

    bool _HasEnoughCash(int cost)
    {
        return cost <= Money;
    }

    void _TryBuyItem(ItemSize size, int cost)
    {
        print("trying to buy item");
        if (_ShopHasSpace(size) && _HasEnoughCash(cost))
        {
            var result = Shop.GetComponent<Shop>().AddItem(size);
            if (result) { Money -= cost; }
            RemainingSpace.text = string.Format("Space: {0}", Shop.GetComponent<Shop>().RemainingSpace);
        }
    }

    void _TryExpandShop()
    {
        //check n pay
        if (Money < EXPANSIONCOST) { return; }
        Money -= EXPANSIONCOST;
        Shop.GetComponent<Shop>().Expand();
        RemainingSpace.text = string.Format("Space: {0}", Shop.GetComponent<Shop>().RemainingSpace);
        //WinGame(); //TODO: check conditions
    }
}
