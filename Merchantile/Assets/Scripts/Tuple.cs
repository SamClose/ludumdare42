﻿using System.Collections.Generic;

public class Tuple<T1, T2>
{
    //public int i;
    //public int j;
    public object Item1;
    public object Item2;

    //public Tuple(int i, int j)
    //{
    //    this.i = i;
    //    this.j = j;
    //}

    public Tuple(object item1, object item2)
    {
        Item1 = item1;
        Item2 = item2;
    }
}