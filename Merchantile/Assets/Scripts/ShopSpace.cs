﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ShopSpaceTile : Tile {

    public ShopSpaceTile()
    {
        IsOwned = false;
        IsOccupied = false;
    }

    public bool IsOwned { get; set; }

    public bool IsOccupied { get; set; } //may want to also hold contents and return this based on that 
}
