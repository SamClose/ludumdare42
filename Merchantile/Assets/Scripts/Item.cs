﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

    public ItemSize Size;
    public int Value { get; set; }
    public List<Tuple<int, int>> Positions { get; set; }
    
	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

}

public enum ItemSize
{
    None,
    Small = 1,
    Medium = 4, //was 2
    Large = 9, //was 4
}
