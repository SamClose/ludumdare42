﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BoxTile : Tile {

    public override void RefreshTile(Vector3Int position, ITilemap tilemap)
    {
        base.RefreshTile(position, tilemap);
    }

    public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
    {
        base.GetTileData(position, tilemap, ref tileData);
        //tileData.sprite = Sprites[1];
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

#if UNITY_EDITOR
    [MenuItem("Assets/Create/Tiles/BoxTile")]
    public static void CreateFloorTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Boxtile", "New Boxtile", "asset", "Save Boxtile", "Assets");
        if (string.IsNullOrEmpty(path))
        {
            return;
        }

        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<BoxTile>(), path);
    }

#endif
}
