﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class FloorTile : Tile {

    [SerializeField]
    public Sprite[] Sprites;

    public override void RefreshTile(Vector3Int position, ITilemap tilemap)
    {
        base.RefreshTile(position, tilemap);
    }

    public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
    {
        base.GetTileData(position, tilemap, ref tileData);
        //tileData.sprite = Sprites[1];
    }

    public void PurchaseArea()
    {
        //sprite = Sprites[1];
        //tileData.sprite = Sprites[1];
    }


#if UNITY_EDITOR
    [MenuItem("Assets/Create/Tiles/FloorTile")]
    public static void CreateFloorTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Floortile", "New Floortile", "asset", "Save floortile", "Assets");
        if (string.IsNullOrEmpty(path))
        {
            return;
        }

        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<FloorTile>(), path);
    }

#endif

}
