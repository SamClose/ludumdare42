﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Shop : MonoBehaviour {

    //height/width of shop at game start
    const int MAX_SHOP_WIDTH = 10;
    const int INITIAL_SHOP_WIDTH = 3;
    int xInit = -5;
    int yInit = 4;
    int currentWidth = INITIAL_SHOP_WIDTH;

    public GameObject FloorTile;
    public GameObject FloorTileDark;
    public Sprite CarpetTileSprite;
    public Sprite BoxTileSprite;
    public Sprite ClearTileSprite;

    public ShopSpaceTile[,] ShopSpace = new ShopSpaceTile[10,10]; //???
    public GameObject Item;
    public GameObject GameManager;

    //tilemaps
    public GameObject Floor;
    public GameObject Crates;

    int MaxSpace
    {
        get
        {
            return (currentWidth * currentWidth) - 2;
        }
    }

    int _remainingSpace = 7;
    public int RemainingSpace
    {
        get
        {
            return _remainingSpace;
        }
        set
        {
            _remainingSpace = value;
        }
    }

    List<Item> _items = new List<Item>();
    public List<Item> Items
    {
        get { return _items; }
        set
        {
            _items = value;
            //_UpdateRemainingSpace();
        }
    }


    void Start () {
        SetupShop(currentWidth);
        //SetupBoxSpace(currentWidth);
    }
	
	void Update () {
		
	}


    public void Expand()
    {
        if(currentWidth >= MAX_SHOP_WIDTH)
        {
            GameManager.GetComponent<GameManager>().WinGame();
        }
        print("Expanding shop");

        var occupiedSpace = MaxSpace - RemainingSpace;
        currentWidth++;
        SetupShop(currentWidth);
        RemainingSpace = MaxSpace - occupiedSpace;
    }


    void SetupShop(int width)
    {
        for (int i = xInit; i < xInit + width; i++)
        {
            for (int j = yInit; j > yInit - width; j--)
            {
                var floor = Floor.GetComponent<Tilemap>();
                var carpetTile = ScriptableObject.CreateInstance("CarpetTile") as CarpetTile;
                carpetTile.sprite = CarpetTileSprite;
                floor.SetTile(new Vector3Int(i, j, 0), carpetTile);
            }
        }

        for (int i = 0; i < MAX_SHOP_WIDTH; i++)
        {
            for (int j = 0; j < MAX_SHOP_WIDTH; j++)
            {
                if (ShopSpace[i,j] == null)
                {
                    ShopSpace[i, j] = new ShopSpaceTile(); //TODO: figure out how to create this correctly
                }
                if (i < currentWidth && j < currentWidth)
                {
                    ShopSpace[i, j].IsOwned = true;
                }
                //ShopSpace[row, column] = (ShopSpaceTile)ScriptableObject.CreateInstance("ShopSpaceTile");
            }
        }

        print("tried to make shop");
    }


    //creates crates at every square in the shop
    //don't use this one. let's create crates in specific spots
    void SetupBoxSpace(int width)
    {
        for (int i = xInit; i < xInit + width; i++)
        {
            for (int j = yInit; j > yInit - width; j--)
            {
                if (i==xInit && j ==yInit || i==xInit && j == yInit - 1) { continue; }
                print("tried it");
                var crateLayer = Crates.GetComponent<Tilemap>();
                var boxTile = ScriptableObject.CreateInstance("BoxTile") as BoxTile;
                boxTile.sprite = BoxTileSprite;
                ShopSpace[i, j].IsOwned = true;
                crateLayer.SetTile(new Vector3Int(i, j, 0), boxTile);
            }
        }
    }

    //Determine Location to place crate
    //start with just picking the first available
    List<Tuple<int,int>> TryFindOpenSpace(ItemSize itemSize)
    {
        switch (itemSize)
        {
            case ItemSize.Small:
                return _FindFirstSingleOpenSpace();
            case ItemSize.Medium:
                return _FindOpenSpacesBySize((int)itemSize);
            case ItemSize.Large:
                return _FindOpenSpacesBySize((int)itemSize);
            default:
                return null;
        }
    }

    List<Tuple<int, int>> _FindFirstSingleOpenSpace()
    {
        for (int y = 0; y < currentWidth; y++)
        {
            for (int x = 0; x < currentWidth; x++)
            {
                print("determining available location");
                print("x: " + x + "; y:" + y);
                if (x == 0 && y == 0 || y == 1 && x == 0) //ignore the 2 default occupied spaces
                {
                    print("skipping over reserved space");
                    continue;
                }
                if (ShopSpace[x, y] == null || !ShopSpace[x, y].IsOccupied) //space is open
                {
                    print("position is empty");
                    return new List<Tuple<int, int>>() { new Tuple<int, int>(x, y) };
                }
            }
        }
        print("Found no available locations");
        return new List<Tuple<int,int>>();
    }
    
    List<Tuple<int,int>> _FindOpenSpacesBySize(int itemSize)
    {
        //TODO: Find a square of available spaces by itemSize
        //find first availablespace, then check the square around it
        //if that space can't create a full space, go to the next square
        //initially I thought this method would be able to reuse the one above but now I think it's better to make something new

        List<Tuple<int, int>> allSpaces = new List<Tuple<int, int>>();

        for (int y = 0; y < currentWidth; y++)
        {
            for (int x = 0; x < currentWidth; x++)
            {
                print("determining available location");
                print("x: " + x + "; y:" + y);
                if (x == 0 && y == 0 || y == 1 && x == 0) //ignore the 2 default occupied spaces
                {
                    print("skipping over reserved space");
                    continue;
                }
                if (ShopSpace[x, y] == null || !ShopSpace[x, y].IsOccupied) //space is open
                {
                    print("position is empty");
                    var tryCreateAtPosition = _CheckPositionFromPoint(new Tuple<int, int>(x, y), itemSize);
                    if ((bool)tryCreateAtPosition.Item1)
                    {
                        allSpaces = (List<Tuple<int, int>>)tryCreateAtPosition.Item2;
                        //if allSpaces.Count == itemSize return;
                        if(allSpaces.Count == itemSize)
                        {
                            return allSpaces;
                        }
                    }
                }
            }
        }

        //TODO: Evaluate what should be returned here
        return null;
    }

    Tuple<bool, List<Tuple<int,int>>> _CheckPositionFromPoint(Tuple<int,int> itemOrigin, int itemSize)
    {
        var canCreate = false;
        List<Tuple<int, int>> positionsList = new List<Tuple<int, int>>();
        
        int itemSizeMod = 0;
        switch (itemSize)
        {
            case 1:
                itemSizeMod = 1;
                break;
            case 4:
                itemSizeMod = 2;
                break;
            case 9:
                itemSizeMod = 3;
                break;
            default:
                break;
        }

        //Item would extend past bound of shop and thus cannot be created
        if ((int)itemOrigin.Item1 + itemSizeMod > MAX_SHOP_WIDTH
            || (int)itemOrigin.Item2 + itemSizeMod > MAX_SHOP_WIDTH)
        {
            print("Item cannot be placed at current position");
            return new Tuple<bool, List<Tuple<int, int>>>(canCreate, positionsList);
        }

        for (int y = (int)itemOrigin.Item2; y < (int)itemOrigin.Item2 + itemSizeMod; y++)
        {
            for (int x = (int)itemOrigin.Item1; x < (int)itemOrigin.Item1 + itemSizeMod; x++)
            {
                //space is available to fill
                if(ShopSpace[x,y] == null)
                {
                    print("shop space not initiated");
                    canCreate = false;
                    return new Tuple<bool, List<Tuple<int, int>>>(canCreate, positionsList);
                }
                //TODO: Improve this logic to short-circuit on first false
                else if (ShopSpace[x,y].IsOwned && !ShopSpace[x, y].IsOccupied)
                {
                    positionsList.Add(new Tuple<int, int>(x, y));
                }
            }
        }

        if(positionsList.Count == itemSize)
        {
            canCreate = true;
        }
        return new Tuple<bool, List<Tuple<int, int>>>(canCreate, positionsList);
    }


    //creates a single crate at a given position
    void CreateCrateAtLocation(int row, int column)
    {
        var crateLayer = Crates.GetComponent<Tilemap>();
        var boxTile = ScriptableObject.CreateInstance("BoxTile") as BoxTile;
        boxTile.sprite = BoxTileSprite;
        print("CreateCrate: "+ "i: " + row + "; j:" + column);
        if(ShopSpace[row, column] == null)
        {
            print("ShopSpace not initialized");
            //print(ShopSpace);
            ShopSpace[row, column] = new ShopSpaceTile(); //TODO: figure out how to create this correctly
            //ShopSpace[row, column] = (ShopSpaceTile)ScriptableObject.CreateInstance("ShopSpaceTile");
        }
        ShopSpace[row, column].IsOccupied = true;
        crateLayer.SetTile(new Vector3Int(row + xInit, yInit - column, 0), boxTile);
    }

    void RemoveCrateAtLocation(int row, int column)
    {
        print("Removing crate");
        var crateLayer = Crates.GetComponent<Tilemap>();
        //might need to create "empty" cell tile
        var clearTile = ScriptableObject.CreateInstance("ClearTile") as ClearTile;
        clearTile.sprite = ClearTileSprite;
        if(ShopSpace[row,column] == null)
        {
            print("ShopSpace not initialized, nothing to remove.");
            //why? but nothing to remove
        }
        ShopSpace[row, column].IsOccupied = false;
        crateLayer.SetTile(new Vector3Int(row + xInit, yInit - column, 0), clearTile);
    }

    public bool AddItem(ItemSize itemSize)
    {
        var positions = TryFindOpenSpace(itemSize);
        if (positions == null || positions.Count == 0)
        {
            print("found no positions");
            return false;
        }
        print("position count: " + positions.Count);
        foreach(var position in positions)
        {
            CreateCrateAtLocation((int)position.Item1, (int)position.Item2);
        }
        //TODO: Pass position along
        var newItem = CreateItem(itemSize, positions);
        Items.Add(newItem);
        _UpdateRemainingSpace(-1 * (int)itemSize);
        return true;
    }

    //TODO: Remove sold crate
    public int SellItem(Item item)
    {
        if(item == null)
        {
            print("Shop.SellItem received null item");
        }
        var actualItem = item.GetComponent<Item>();
        print("actualItem pos: " + actualItem.Positions.Count);
        var size = actualItem.Size;
        var cost = actualItem.Value;
        print("selling item");
        foreach(var position in actualItem.Positions)
        {
            print("position: " + (int)position.Item1 + ", " + (int)position.Item2);
            //var a = ShopSpace[(int)position.Item1, (int)position.Item2];
            RemoveCrateAtLocation((int)position.Item1, (int)position.Item2);
        }
        Items.Remove(item);
        Destroy(item);
        //Destroy(item.gameObject);
        _UpdateRemainingSpace((int)size);
        print("item sold");
        return cost;
    }

    int SetItemValue(ItemSize itemSize)
    {
        switch (itemSize)
        {
            case ItemSize.None:
                return 0;
            case ItemSize.Small:
                return 15;
            case ItemSize.Medium:
                return 40;
            case ItemSize.Large:
                return 85;
            default:
                print("ItemSize not found" + itemSize);
                return 0;
        }
    }

    float Offset(int increment)
    {
        return (float)increment;
    }

    void _UpdateRemainingSpace(int spaceChange)
    {
        print("max: " + MaxSpace);
        print("space: " + RemainingSpace);
        print("change: " + spaceChange);
        RemainingSpace += spaceChange;
    }

    //TODO: Pass in location and store it
    Item CreateItem(ItemSize size, List<Tuple<int,int>> positions)
    {
        var newItem = Instantiate(Item);
        var actualItem = newItem.GetComponent<Item>();
        //set all fields
        actualItem.Size = size;
        actualItem.Value = SetItemValue(size);
        actualItem.Positions = positions;
        //return newItem;
        return actualItem;
    }
}